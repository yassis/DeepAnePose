absl-py==1.0.0
antspyx==0.3.1
anyio==3.6.1
argon2-cffi==21.3.0
argon2-cffi-bindings==21.2.0
attrs==22.1.0
Babel==2.10.3
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
bleach==5.0.1
brotlipy==0.7.0
cachetools==4.2.4
certifi==2021.10.8
cffi @ file:///tmp/build/80754af9/cffi_1625814693446/work
chardet @ file:///tmp/build/80754af9/chardet_1607706768982/work
charset-normalizer==2.0.8
chart-studio==1.1.0
click==8.1.3
conda==4.10.3
conda-build==3.21.5
conda-package-handling @ file:///tmp/build/80754af9/conda-package-handling_1618262151086/work
configparser==5.3.0
connected-components-3d==3.2.0
cycler==0.11.0
debugpy==1.6.3
decorator @ file:///tmp/build/80754af9/decorator_1632776554403/work
defusedxml==0.7.1
Deprecated==1.2.13
dnspython==2.1.0
docker-pycreds==0.4.0
einops==0.6.0
entrypoints==0.4
fastjsonschema==2.16.2
filelock @ file:///home/linux1/recipes/ci/filelock_1610993975404/work
fonttools==4.28.2
future==0.18.2
gitdb==4.0.9
GitPython==3.1.27
glob2 @ file:///home/linux1/recipes/ci/glob2_1610991677669/work
google-auth==2.3.3
google-auth-oauthlib==0.4.6
grpcio==1.42.0
humanize==4.2.3
idna==3.3
imageio==2.13.1
importlib-metadata==4.8.2
importlib-resources==5.10.0
indexed-gzip==1.6.4
ipykernel==6.16.0
ipympl==0.9.1
ipython @ file:///tmp/build/80754af9/ipython_1632141056704/work
ipython-genutils==0.2.0
ipywidgets==7.7.0
jedi==0.17.2
Jinja2==3.1.2
joblib==1.1.0
json5==0.9.10
jsonschema==4.16.0
jupyter-core==4.11.1
jupyter-server==1.19.1
jupyter_client==7.3.5
jupyterlab==3.4.8
jupyterlab-pygments==0.2.2
jupyterlab-widgets==1.1.0
jupyterlab_server==2.15.2
kiwisolver==1.3.2
libarchive-c @ file:///tmp/build/80754af9/python-libarchive-c_1617780486945/work
llvmlite==0.38.1
Mako==1.2.0
Markdown==3.3.6
MarkupSafe @ file:///tmp/build/80754af9/markupsafe_1621528142364/work
matplotlib==3.5.0
matplotlib-inline @ file:///tmp/build/80754af9/matplotlib-inline_1628242447089/work
medcam==0.1.21
mistune==2.0.4
mkl-fft==1.3.1
mkl-random @ file:///tmp/build/80754af9/mkl_random_1626179032232/work
mkl-service==2.4.0
nbclassic==0.4.5
nbclient==0.7.0
nbconvert==7.2.1
nbformat==5.6.1
nest-asyncio==1.5.6
networkx==2.6.3
nibabel==3.2.1
notebook==6.4.12
notebook-shim==0.1.0
numba==0.55.2
numexpr==2.7.3
numpy==1.21.4
oauthlib==3.1.1
olefile==0.46
packaging==21.3
pandas==1.3.4
pandocfilters==1.5.0
parso==0.7.1
pathtools==0.1.2
patsy==0.5.2
pexpect @ file:///tmp/build/80754af9/pexpect_1605563209008/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow==8.4.0
pkginfo==1.7.1
pkgutil_resolve_name==1.3.10
plotly==5.6.0
prefetch-generator==1.0.1
prometheus-client==0.14.1
promise==2.3
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1633440160888/work
protobuf==3.19.1
psutil @ file:///tmp/build/80754af9/psutil_1612298016854/work
ptyprocess @ file:///tmp/build/80754af9/ptyprocess_1609355006118/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycosat==0.6.3
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
Pygments @ file:///tmp/build/80754af9/pygments_1629234116488/work
pynndescent==0.5.7
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1608057966937/work
pyparsing==3.0.6
pyrsistent==0.18.1
PySocks @ file:///tmp/build/80754af9/pysocks_1594394576006/work
python-abc==0.1.0
python-dateutil==2.8.2
python-etcd==0.4.5
pytz==2021.3
PyWavelets==1.2.0
PyYAML==5.4.1
pyzmq==24.0.1
requests==2.26.0
requests-oauthlib==1.3.0
retrying==1.3.3
rsa==4.8
ruamel-yaml-conda @ file:///tmp/build/80754af9/ruamel_yaml_1616016701961/work
scikit-image==0.18.3
scikit-learn==1.0.1
scipy==1.7.3
Send2Trash==1.8.0
sentry-sdk==1.5.12
setproctitle==1.2.3
setuptools-scm==6.3.2
shortuuid==1.0.9
SimpleITK==2.0.2
six @ file:///tmp/build/80754af9/six_1623709665295/work
sklearn==0.0
smmap==5.0.0
sniffio==1.3.0
soupsieve @ file:///tmp/build/80754af9/soupsieve_1616183228191/work
statsmodels==0.13.2
tables==3.6.1
tenacity==8.0.1
tensorboard==2.7.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.0
tensorboardX==2.4.1
terminado==0.16.0
threadpoolctl==3.0.0
tifffile==2021.11.2
tinycss2==1.1.1
tomli==1.2.2
torch==1.10.0
torchtext==0.11.0
tornado==6.2
tqdm==4.62.3
traitlets==5.4.0
typing_extensions==4.0.1
urllib3==1.26.7
wcwidth @ file:///Users/ktietz/demo/mc3/conda-bld/wcwidth_1629357192024/work
webcolors==1.11.1
webencodings==0.5.1
websocket-client==1.4.1
Werkzeug==2.0.2
widgetsnbextension==3.6.0
wordcloud==1.8.2.2
wrapt==1.14.1
zipp==3.6.0
